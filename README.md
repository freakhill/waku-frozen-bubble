FrozenBubble with the WAKU WAKU SDK
===================================

This repository is a sample integration of the WAKU WAKU SDK into an open source
game, FrozenBubble. It shows a finished integration to help the SDK users in
their own integration work.

Missing dependencies
--------------------

For this repository to compile and run, you need to add a `libs` directory that
contains two JAR files:

* wakuwaku-sdk.jar, either from WAKU WAKU's web site, or in the [SDK's repository](https://bitbucket.org/freakhill/wakuwaku-android-sdk)
* android-support-v4.jar, either from Google, or in the SDK's repository as well.

Original README
---------------

The Android port of the Frozen Bubble game.  Developed with SDK v. 1.1_r1.
The code is based on the Java version of Frozen Bubble created by Glenn
Sanson.  The original Frozen Bubble was created by Guillaume Cottenceau
(programming), Alexis Younes and Amaury Amblard-Ladurantie (artwork) and
Matthias Le Bidan (soundtrack).

The Android port, just like the original Frozen Bubble, is covered by
GNU GPL v2.

